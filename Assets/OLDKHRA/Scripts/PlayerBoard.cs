﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBoard : MonoBehaviour
{
    private Rigidbody _rigidbody;
    public float speed = 10.0f;
    public bool yerde;
    public GameObject[] wheels = new GameObject[4];
    void Start()
    {
        _rigidbody = this.GetComponent<Rigidbody>();
        yerde = true;
    }

    // Update is called once per frame
    void Update()
    {
        //_rigidbody.AddForce(new Vector3(-speed, 0, 0.0f));
        if (yerde == true)
        {
            /*wheels[2].GetComponent<Rigidbody>().AddForce(new Vector3(-1.0f, -3.0f, 0.0f));
            wheels[3].GetComponent<Rigidbody>().AddForce(new Vector3(-1.0f, -3.0f, 0.0f));*/
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
            {
                if (Input.GetKey(KeyCode.LeftArrow)) { _rigidbody.AddForce(new Vector3(0.0f, 0.0f, -speed)); }
                else { _rigidbody.AddForce(new Vector3(0.0f, 0.0f, speed)); }
                yerde = false;

            }
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
            {
                if (Input.GetKey(KeyCode.UpArrow)) { _rigidbody.AddForce(new Vector3(-speed*10, -speed, 0.0f)); }
                else { _rigidbody.AddForce(new Vector3(speed, speed, 0.0f)); }
                yerde = false;

            }
        }
    }
    private void move()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            // _rigidbody.AddForce(transform.right * 100.0f);
            _rigidbody.AddForce(new Vector3(50,0,0));
            //_rigidbody.MovePosition(_rigidbody.transform.position + new Vector3(0, 0, 1) * 17 * Time.deltaTime);
            //_rigidbody.velocity= new Vector3(5, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            _rigidbody.AddForce(new Vector3(0, 0, 50));
        }
    }
    private void OnCollisionStay(Collision other)
    {
        if (gameObject.tag=="wheel1")
        {
            yerde = true;
        }
        else if (gameObject.tag == "wheel2")
        {
            yerde = true;
        }
        else if (gameObject.tag == "wheel3")
        {
            yerde = true;
        }
        else if (gameObject.tag == "wheel4")
        {
            yerde = true;
        }
    }
}
