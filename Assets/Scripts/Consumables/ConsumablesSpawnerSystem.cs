using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumablesSpawnerSystem : MonoBehaviour
{
    [SerializeField]
    protected Transform startPos, endPos;
    [SerializeField]
    protected GameObject[] consumablesGamesobjects;
    protected GameObject lastGameobject,allSpawnedConsumables;
    protected bool consumablesSpawned;
    protected Vector3 spawnPos;
    protected int i=0;
    void Start()
    {
        allSpawnedConsumables = new GameObject("allSpawnedConsumables");
        randomConsumableSpawn();
        InvokeRepeating("spawnAllConsumables" , 0.1f, 0.1f);
    }

    protected void spawnAllConsumables()
    {
        if (!endPosReached(lastGameobject))
        {
            randomConsumableSpawn();
        }
        else
        {
            CancelInvoke("spawnAllConsumables");
        }
    }

    protected bool endPosReached(GameObject lastGameobject)
    {
        if (lastGameobject.transform.position.z >= endPos.position.z)
        {
            return true;
        }
        else if (lastGameobject==null)
        {
            return true;
        }
        return false;
    }

    protected void randomConsumableSpawn()
    {
        i += Random.Range(20,30);
        float randomXpos = Random.Range(-4, 20);
        spawnPos = new Vector3(randomXpos, startPos.position.y+10, startPos.position.z + i);
        int randomRandomizer = Random.Range(0, 6);
        switch(randomRandomizer)
        {
            case 0:
                lastGameobject = Instantiate(consumablesGamesobjects[0], spawnPos, Quaternion.identity);
                lastGameobject.transform.SetParent(allSpawnedConsumables.transform);
                return;
            case 1:
                lastGameobject = Instantiate(consumablesGamesobjects[0], spawnPos, Quaternion.identity);
                lastGameobject.transform.SetParent(allSpawnedConsumables.transform);
                return;
            case 2:
                lastGameobject = Instantiate(consumablesGamesobjects[0], spawnPos, Quaternion.identity);
                lastGameobject.transform.SetParent(allSpawnedConsumables.transform);
                return;
            case 3:
                lastGameobject = Instantiate(consumablesGamesobjects[1], spawnPos, Quaternion.identity);
                lastGameobject.transform.SetParent(allSpawnedConsumables.transform);
                return;
            case 4:
                lastGameobject = Instantiate(consumablesGamesobjects[1], spawnPos, Quaternion.identity);
                lastGameobject.transform.SetParent(allSpawnedConsumables.transform);
                return;
            case 5:
                lastGameobject = Instantiate(consumablesGamesobjects[2], spawnPos, Quaternion.identity);
                lastGameobject.transform.SetParent(allSpawnedConsumables.transform);
                return;
            default:
                lastGameobject = Instantiate(consumablesGamesobjects[0], spawnPos, Quaternion.identity);
                lastGameobject.transform.SetParent(allSpawnedConsumables.transform);
                break;
        }
        
        //lastGameobject =  Instantiate(consumablesGamesobjects[randomPick], spawnPos, Quaternion.identity);

    }
}
