using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumablesAnimationLifter : MonoBehaviour
{
    private float howMuchToWait = 2.0f, ratioLifter=250f;
    private Rigidbody consumableRigidbody;
    void Start()
    {
        consumableRigidbody = GetComponent<Rigidbody>();
        StartCoroutine(liftTheConsumableUp());  
    }

    
    void Update()
    {
        
    }

    IEnumerator liftTheConsumableUp()
    {
        while(true)
        {
            //transform.Translate(Vector3.up * ratioLifter*Time.deltaTime,Space.World);
            consumableRigidbody.AddForce(Vector3.up * ratioLifter);
            yield return new WaitForSeconds(howMuchToWait);
            //transform.Translate(-Vector3.up * ratioLifter);
        }
    }
}
