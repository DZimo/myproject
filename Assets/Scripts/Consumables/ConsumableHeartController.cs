using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoulmaAllFunctions;

public class ConsumableHeartController : MonoBehaviour
{
    public RoulmaController roulmaController;
    private MeshRenderer ConsumableHeartMeshRenderer;
    private void Start()
    {
        //roulmaController = GetComponent<RoulmaController>();
        ConsumableHeartMeshRenderer = gameObject.GetComponent < MeshRenderer>();
    }
    void LateUpdate()
    {
        
        if (RoulmaController.roulmaData.roulmaVariables.roulmaPlayerStats.totalOfLife == 2)
        {
            ConsumableHeartMeshRenderer.enabled = false;
        }
        else
        {
            ConsumableHeartMeshRenderer.enabled = true;
        }
    }
}
