using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSpawnerSystem : MonoBehaviour
{
    [SerializeField]
    private GameObject newMapToSpawn, oldMapToDelete;
    public List<GameObject> allOldMapsToDelete = new List<GameObject>();
    private Vector3 whereToSpawn;
    private int positionToChangeY=50, positionToChangeZ = 2500;
    private float timeToDelete = 120f;
    void Start()
    {
        InvokeRepeating("deleteOldMaps", timeToDelete, timeToDelete);
        //spawnMethod();
    }

    public void spawnNewMap()
    {
        allOldMapsToDelete.Add(oldMapToDelete);
        whereToSpawn = new Vector3(oldMapToDelete.transform.position.x, oldMapToDelete.transform.position.y - positionToChangeY, oldMapToDelete.transform.position.z + positionToChangeZ);
        oldMapToDelete = Instantiate(newMapToSpawn, whereToSpawn, Quaternion.identity);  
        transform.position = new Vector3(transform.position.x, transform.position.y - positionToChangeY, transform.position.z + positionToChangeZ);
        /*endPos.transform.position = new Vector3(endPos.transform.position.x, endPos.transform.position.y - positionToChangeY, endPos.transform.position.z + positionToChangeZ);
        startPos.transform.position = new Vector3(startPos.transform.position.x, startPos.transform.position.y - positionToChangeY, startPos.transform.position.z + positionToChangeZ);*/
        //Destroy(allSpawnedConsumables);
        //spawnMethod();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("RoulmaBoard"))
        {
            spawnNewMap();
        }
    }

    private void deleteOldMaps()
    {
        int i = allOldMapsToDelete.Count;
        for (int j = 0; j < i - 1; j++)
        {
            Destroy(allOldMapsToDelete[j]);
        }
        allOldMapsToDelete.RemoveRange(0, i - 1);
    }

    private void spawnMethod()
    {
        //allSpawnedConsumables = new GameObject("allSpawnedConsumables");
        //randomConsumableSpawn();
        //InvokeRepeating("spawnAllConsumables", 0.1f, 0.1f);
    }
}