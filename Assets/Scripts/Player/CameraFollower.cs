using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    [SerializeField] private GameObject playerGameObject;
    private Vector3 cameraStartPos,differenceBetween, lookFromAway;
    private float howMuchLookFromAway=10f;
    void Start()
    {
        cameraStartPos = transform.position;
        differenceBetween = playerGameObject.transform.position - cameraStartPos;
    }

    void LateUpdate()
    {
        followThePlayer();
    }
    private void followThePlayer()
    {
        transform.position = playerGameObject.transform.position - differenceBetween;
    }

    private void startAnimationShake()
    {

    }

    private void deathAnimationShake()
    {
        lookFromAway = new Vector3(transform.position.x,transform.position.y, transform.position.z- howMuchLookFromAway);
        transform.position = lookFromAway;
    }

    void OnPreRender()
    {
        GL.wireframe = true;    
    }

    void OnPostRender()
    {
        GL.wireframe = true;
    }
}
