using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


[System.Serializable]
public class RoulmaData : MonoBehaviour
{
    [System.Serializable]
    public struct RoulmaVariablesStruct
    {
        public RoulmaSpeedVariables roulmaSpeedVariables;
        public AnimationParticlesStruct roulmaAnimationParticles;
        public RoulmaCanvasText roulmaCanvasText;
        public RoulmaPlayerBooleans roulmaPlayerBooleans;
        public RoulmaPlayerStats roulmaPlayerStats;
        public RoulmaCanvasButtons roulmaCanvasButtons;
        public RoulmaCanvasGameobjects roulmaCanvasGameobjects;
        public Joystick joystick;
    }
    public RoulmaVariablesStruct roulmaVariables;

    [System.Serializable]
    public class AnimationParticlesStruct
    {
        public ParticleSystem destroyBombParticle, destroyBombSmokeParticle, destroyCoinParticle, destroyHeartParticle, playerWinParticle;
    }
    //public AnimationParticlesStruct animationParticlesStruct;

    [System.Serializable]
    public class RoulmaSpeedVariables
    {
        public float turnSpeed = 2.0f, turnForce = 10f, accelerationSpeed = 7000.0f, realTimeSpeed, rpm, minimumSpeedToTurn = 10f, maximumSpeedInKmh = 100f,
        maximumAngleOfRotationY = 15f, originalAngleOfRotationY = 360f, ReduceTorqueForceFloat = 2.5f, roulmaStartPosZ;
        public GameObject centerOfMass;
        public Rigidbody roulmaRigidbody, protagonistRigidbody;
        public List<WheelCollider> listOfWheels;
        public List<Transform> listOfRoulmaParts;
        public int wheelsOnGround;
    }

    [System.Serializable]
    public class RoulmaCanvasText
    {
        public TextMeshProUGUI speedometerText, rpmText, playerIsDeadText, playerhasWonText, totalOfCoinsText, startGameDelayText, totalMetersCountText;
    }

    [System.Serializable]
    public class RoulmaPlayerBooleans
    {
        public bool playerIsDead, playerhasWon, wantsToTurnRight, startGameDelayaIsOver;
    }

    [System.Serializable]
    public class RoulmaPlayerStats
    {
        public int totalOfCoins = 0, totalOfLife = 1, scoreToAdd = 25;
    }

    [System.Serializable]
    public class RoulmaCanvasButtons
    {
        public Button replayTheGameButton, accelerateRoulmaBUtton, TurnRoulmaLefttButton, TurnRoulmaRightButton;
    }

    [System.Serializable]
    public class RoulmaCanvasGameobjects
    {
        public GameObject[] playerHeartsCanvas;
    }

}