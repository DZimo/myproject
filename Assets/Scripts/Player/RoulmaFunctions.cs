using UnityEngine;
using UnityEngine.SceneManagement;

namespace RoulmaAllFunctions
{
    public class RoulmaFunctions : RoulmaStatsFunctions
    {
        /*protected RoulmaAnimations roulmaAnimations;
        protected RoulmaData roulmaData;
        protected RoulmaFunctions roulmaFunctions;*/
        public void moveTheRoulma()
        {
            float HorizontalInput = roulmaData.roulmaVariables.joystick.Horizontal;
            float SpeedInKmh = roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.velocity.magnitude * 3.6f;
            float accelerationSpeed = roulmaData.roulmaVariables.roulmaSpeedVariables.accelerationSpeed;
            float minimumSpeedToTurn = roulmaData.roulmaVariables.roulmaSpeedVariables.minimumSpeedToTurn;
            float turnForce = roulmaData.roulmaVariables.roulmaSpeedVariables.turnForce;
            float turnSpeed = roulmaData.roulmaVariables.roulmaSpeedVariables.turnSpeed;
            float reduceTorqueForceFloat = roulmaData.roulmaVariables.roulmaSpeedVariables.ReduceTorqueForceFloat;
            float realTimeSpeed = roulmaData.roulmaVariables.roulmaSpeedVariables.realTimeSpeed;
            //Check if the player can rotate
            bool canRotate = !(transform.localRotation.eulerAngles.y > roulmaData.roulmaVariables.roulmaSpeedVariables.maximumAngleOfRotationY && transform.localRotation.eulerAngles.y < roulmaData.roulmaVariables.roulmaSpeedVariables.originalAngleOfRotationY - roulmaData.roulmaVariables.roulmaSpeedVariables.maximumAngleOfRotationY);
            // IF IT'S A PC 
            if (Application.platform != RuntimePlatform.Android)
            {
                moveTheRoulmaForward(realTimeSpeed, accelerationSpeed);
                // Apply Torque to wheels to make them looks like moving
                if (SpeedInKmh > 0)
                {
                    applyTorqueToWheelsAnimation(SpeedInKmh);
                }

                // Move the roulma when it is on the ground
                if (isGrounded())
                {
                    moveTheRoulmaRightOrLeft(HorizontalInput, SpeedInKmh, minimumSpeedToTurn, canRotate, turnSpeed, turnForce, accelerationSpeed, reduceTorqueForceFloat);
                }
                // Move the roulma slowly when it is not on the ground and flying
                else
                {
                    moveTheRoulmaRightOrLeft(HorizontalInput, SpeedInKmh, minimumSpeedToTurn, canRotate, turnSpeed / reduceTorqueForceFloat, turnForce, accelerationSpeed, reduceTorqueForceFloat);
                }
            }
            else
            {
                moveTheRoulmaForward(realTimeSpeed, accelerationSpeed);
                // Apply Torque to wheels to make them looks like moving
                if (SpeedInKmh > 0)
                {
                    applyTorqueToWheelsAnimation(SpeedInKmh);
                }

                // Move the roulma when it is on the ground
                if (isGrounded())
                {
                    moveTheRoulmaRightOrLeft(HorizontalInput, SpeedInKmh, minimumSpeedToTurn, canRotate, turnSpeed, turnForce, accelerationSpeed, reduceTorqueForceFloat);
                }
                // Move the roulma slowly when it is not on the ground and flying
                else
                {
                    moveTheRoulmaRightOrLeft(HorizontalInput, SpeedInKmh, minimumSpeedToTurn, canRotate, turnSpeed / reduceTorqueForceFloat, turnForce, accelerationSpeed, reduceTorqueForceFloat);
                }
            }
        }

        public void moveTheRoulmaRightOrLeft(float HorizontalInput, float SpeedInKmh, float minimumSpeedToTurn, bool canRotate, float turnSpeed, float turnForce, float accelerationSpeed, float reduceTorqueForceFloat)
        {
            if (SpeedInKmh > minimumSpeedToTurn && canRotate)
            {
                // MOVE THE PLAYER LEFT
                if (HorizontalInput < 0)
                {
                    moveTheRoulmaRight(HorizontalInput, turnSpeed, turnForce);
                    moveTheRoulmaForward(SpeedInKmh, -accelerationSpeed /  reduceTorqueForceFloat);
                }
                // MOVE THE PLAYER RIGHT
                else if (HorizontalInput > 0)
                {
                    moveTheRoulmaLeft(HorizontalInput, turnSpeed, turnForce);
                    moveTheRoulmaForward(SpeedInKmh, -accelerationSpeed / reduceTorqueForceFloat);
                }
            }
        }

        public void moveTheRoulmaRight(float HorizontalInput, float turnSpeed, float turnForce)
        {
            //transform.eulerAngles = new Vector3(transform.eulerAngles.x,Mathf.Clamp(transform.eulerAngles.y, roulmaVariables.originalAngleOfRotationY - roulmaVariables.maximumAngleOfRotationY, roulmaVariables.maximumAngleOfRotationY ), transform.eulerAngles.z) ;
            //roulmaVariables.roulmaRigidbody.AddRelativeTorque(new Vector3(0, 1, 0) * HorizontalInput * roulmaVariables.turnSpeed, ForceMode.Impulse);

            transform.Rotate(Vector3.up * HorizontalInput * turnSpeed * Time.deltaTime);
            transform.Translate(Vector3.right * HorizontalInput* Time.deltaTime * turnForce);
        }

        public void moveTheRoulmaLeft(float HorizontalInput, float turnSpeed, float turnForce)
        {
            //roulmaVariables.roulmaRigidbody.AddRelativeTorque(new Vector3(0, 1, 0) * HorizontalInput * roulmaVariables.turnSpeed, ForceMode.Impulse);
            //roulmaVariables.roulmaRigidbody.ResetInertiaTensor();

            transform.Rotate(Vector3.up * HorizontalInput * turnSpeed * Time.deltaTime);
            transform.Translate(Vector3.right * HorizontalInput * Time.deltaTime * turnForce );
        }

        public void moveTheRoulmaForward(float SpeedInKmh, float accelerationSpeed)
        {
            float maximumSpeedInKmh = roulmaData.roulmaVariables.roulmaSpeedVariables.maximumSpeedInKmh;
            if (SpeedInKmh < maximumSpeedInKmh)
            {
                roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.AddRelativeForce(Vector3.forward * accelerationSpeed);
                //transform.Translate(-Vector3.forward * horsePower * VerticalInput*Time.deltaTime);
            }

            /*if (!canRotate)
            {
                // When the player Y position is more closer to the maximum angle than the difference between the original and maximum angle
                if (transform.localRotation.eulerAngles.y - roulmaVariables.maximumAngleOfRotationY < -(transform.localRotation.eulerAngles.y - (roulmaVariables.originalAngleOfRotationY - roulmaVariables.maximumAngleOfRotationY)))
                {
                    //transform.localEulerAngles = new Vector3(transform.localRotation.eulerAngles.x, roulmaVariables.maximumAngleOfRotationY-1, transform.localRotation.eulerAngles.z);
                    roulmaVariables.roulmaRigidbody.AddRelativeTorque(-new Vector3(0, 1, 0)  * 50f * roulmaVariables.turnSpeed);
                }
                // When the player Y position is more closer to the difference between the original and maximum angle than the the maximum angle
                else if (transform.localRotation.eulerAngles.y - roulmaVariables.maximumAngleOfRotationY > transform.localRotation.eulerAngles.y - (roulmaVariables.originalAngleOfRotationY - roulmaVariables.maximumAngleOfRotationY))
                {
                    // transform.localEulerAngles = new Vector3(transform.localRotation.eulerAngles.x, (roulmaVariables.originalAngleOfRotationY - roulmaVariables.maximumAngleOfRotationY)+1, transform.localRotation.eulerAngles.z);
                    roulmaVariables.roulmaRigidbody.AddRelativeTorque(new Vector3(0, 1, 0) * 50f * roulmaVariables.turnSpeed);
                }
                //roulmaRigidbody.AddTorque(Vector3.up * HorizontalInput * turnSpeed);
            }*/
        }

        private void CounterWheels()
        {
            roulmaData.roulmaVariables.roulmaSpeedVariables.wheelsOnGround = 0;
            foreach (var wheel in roulmaData.roulmaVariables.roulmaSpeedVariables.listOfWheels)
            {
                if (wheel.isGrounded) { roulmaData.roulmaVariables.roulmaSpeedVariables.wheelsOnGround++; }
            }
        }

        private bool isGrounded()
        {
            CounterWheels();
            if (roulmaData.roulmaVariables.roulmaSpeedVariables.wheelsOnGround >= 1)
            {
                return true;
            }
            return false;
        }

        public void exploseThePlayer()
        {
            float xWheels, yWheels, zWheels;
            float turnForce = roulmaData.roulmaVariables.roulmaSpeedVariables.turnForce;
            var wheels = roulmaData.roulmaVariables.roulmaSpeedVariables.listOfWheels;
            var parts = roulmaData.roulmaVariables.roulmaSpeedVariables.listOfRoulmaParts;
            int wheelsMax = 7;
            xWheels = Random.Range(-wheelsMax, wheelsMax); yWheels = Random.Range(-wheelsMax, wheelsMax); zWheels = Random.Range(-wheelsMax, wheelsMax);
            Vector3 whereToThrowWheels = new Vector3(xWheels, yWheels, zWheels);

            // EXPLOSE MAIN RIGIDBODY
            float explosingPlayerForce = 8000f, explosingPlayerRadius = 10f, explosingPlayerModifier = explosingPlayerRadius / 2f, childrenRigidbodyMass = 100f ;
            roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.constraints = RigidbodyConstraints.None;
            roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.angularDrag = 0;
            roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.AddExplosionForce(explosingPlayerForce, new Vector3(transform.position.x + xWheels, transform.position.y + yWheels, transform.position.z + zWheels), explosingPlayerRadius, explosingPlayerModifier, ForceMode.Impulse);
            roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.AddRelativeTorque(whereToThrowWheels * turnForce, ForceMode.Impulse);

            // EXPLOSE THE PROTAGONIST
            roulmaData.roulmaVariables.roulmaSpeedVariables.protagonistRigidbody.isKinematic = false;
            
            // EXPLOSE ALL PARTS
            //Transform[] childrens = gameObject.GetComponentsInChildren<Transform>();
            foreach (var part in parts)
            {
                if (part.gameObject.GetComponent<Rigidbody>() == null)
                {
                    Rigidbody childrenRigidbody = part.gameObject.AddComponent<Rigidbody>();
                    MeshCollider childrenmesh = part.gameObject.AddComponent<MeshCollider>();
                    childrenmesh.convex = true;
                    childrenRigidbody.mass = childrenRigidbodyMass;
                }
            }

            // EXPLOSE ALL WHEELS
            foreach (var wheel in wheels)
            {
                if (wheel.gameObject.GetComponent<Rigidbody>() == null)
                {
                    Rigidbody wheelsRigidbody = wheel.gameObject.AddComponent<Rigidbody>();
                    Destroy(wheel.gameObject.GetComponent<WheelCollider>());
                    MeshCollider wheelsMeshCollider = wheel.gameObject.AddComponent<MeshCollider>();
                    wheelsMeshCollider.convex = true;
                    wheelsRigidbody.mass = childrenRigidbodyMass;
                    wheelsRigidbody.AddExplosionForce(explosingPlayerForce, new Vector3(transform.position.x + xWheels, transform.position.y + yWheels, transform.position.z + zWheels), explosingPlayerRadius, explosingPlayerRadius, ForceMode.Impulse);
                }
            }
        }

        private void initialiseAllVariables()
        {

        }
    }

    public class RoulmaAnimations : RoulmaController
    {
        public void applyTorqueToWheelsAnimation(float SpeedInKmh)
        {
            foreach (var wheel in roulmaData.roulmaVariables.roulmaSpeedVariables.listOfWheels)
            {
                wheel.transform.Rotate(-Vector3.right * SpeedInKmh / 10);
            }
        }

        public void destroyBombAnimation(Vector3 positionOfBomb)
        {
            roulmaData.roulmaVariables.roulmaAnimationParticles.destroyBombParticle.Play();
            roulmaData.roulmaVariables.roulmaAnimationParticles.destroyBombParticle.transform.position = positionOfBomb;
        }

        public void activatePreDestroyAnimation(Vector3 positionOfBomb)
        {
            roulmaData.roulmaVariables.roulmaAnimationParticles.destroyBombSmokeParticle.Play();
        }

        public void deActivatePreDestroyAnimation(Vector3 positionOfBomb)
        {
            roulmaData.roulmaVariables.roulmaAnimationParticles.destroyBombSmokeParticle.Stop();
        }

        public void destroyCoinAnimation(Vector3 positionOfCoin)
        {
            roulmaData.roulmaVariables.roulmaAnimationParticles.destroyCoinParticle.Play();
            roulmaData.roulmaVariables.roulmaAnimationParticles.destroyCoinParticle.transform.position = positionOfCoin;
        }

        public void destroyHeartAnimation(Vector3 positionOfCoin)
        {
            roulmaData.roulmaVariables.roulmaAnimationParticles.destroyHeartParticle.Play();
            roulmaData.roulmaVariables.roulmaAnimationParticles.destroyHeartParticle.transform.position = positionOfCoin;
        }

        public void playerWinAnimation()
        {
            roulmaData.roulmaVariables.roulmaAnimationParticles.playerWinParticle.Play();
            //roulmaData.roulmaVariables.roulmaAnimationParticles.playerWinParticle.transform.position = positionOfPlayer;
            //roulmaData.roulmaVariables.roulmaAnimationParticles.playerWinParticle.transform.position = new Vector3(positionOfPlayer.x, positionOfPlayer.y, positionOfPlayer.z * 10f);
        }
    }

    public class RoulmaStatsFunctions : RoulmaAnimations
    {
        public void addOneCoinToTotal()
        {
            roulmaData.roulmaVariables.roulmaPlayerStats.totalOfCoins += roulmaData.roulmaVariables.roulmaPlayerStats.scoreToAdd;
            roulmaData.roulmaVariables.roulmaCanvasText.totalOfCoinsText.text = roulmaData.roulmaVariables.roulmaPlayerStats.totalOfCoins.ToString();
        }

        public void addOneLifeFromTotal()
        {
            if (roulmaData.roulmaVariables.roulmaPlayerStats.totalOfLife < 2)
            {
                int actualLife=roulmaData.roulmaVariables.roulmaPlayerStats.totalOfLife++;
                roulmaData.roulmaVariables.roulmaCanvasGameobjects.playerHeartsCanvas[actualLife].SetActive(true);
            }

        }

        public void removeOneLifeFromTotal()
        {
            if (roulmaData.roulmaVariables.roulmaPlayerStats.totalOfLife > 1)
            {
                int actualLife=roulmaData.roulmaVariables.roulmaPlayerStats.totalOfLife--;
                roulmaData.roulmaVariables.roulmaCanvasGameobjects.playerHeartsCanvas[actualLife-1].SetActive(false);
            }
            else
            {
                int actualLife = roulmaData.roulmaVariables.roulmaPlayerStats.totalOfLife--;
                roulmaData.roulmaVariables.roulmaCanvasGameobjects.playerHeartsCanvas[actualLife-1].SetActive(false);
                killThePlayer();
            }
        }

        public void activateReplayScreen()
        {
            roulmaData.roulmaVariables.roulmaCanvasButtons.replayTheGameButton.gameObject.SetActive(true);
        }

        public void replayTheGame()
        {
            SceneManager.LoadScene("RoulmaBoardGame");
        }

        public void killThePlayer()
        {
            activateDeathScreen();
            activateReplayScreen();
            roulmaFunctions.exploseThePlayer();
        }

        public void winThePlayer()
        {
            activateWinScreen();
            activateReplayScreen();
        }

        public void activateDeathScreen()
        {
            roulmaData.roulmaVariables.roulmaCanvasText.playerIsDeadText.gameObject.SetActive(true);
            roulmaData.roulmaVariables.roulmaPlayerBooleans.playerIsDead = true;
            activateReplayScreen();
        }

        public void activateWinScreen()
        {
            roulmaData.roulmaVariables.roulmaCanvasText.playerhasWonText.gameObject.SetActive(true);
            roulmaData.roulmaVariables.roulmaPlayerBooleans.playerhasWon = true;
            activateReplayScreen();
        }

        public void CalculateSpeed()
        {
            roulmaData.roulmaVariables.roulmaSpeedVariables.realTimeSpeed = Mathf.Round(roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.velocity.magnitude * 3.6f);
        }

        public void CalculateRpm()
        {
            roulmaData.roulmaVariables.roulmaSpeedVariables.rpm = Mathf.RoundToInt(roulmaData.roulmaVariables.roulmaSpeedVariables.realTimeSpeed / 40);
        }
        public void CalculateMeters()
        {
            int startPos = (int)roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaStartPosZ;
            int currentPos= (int)transform.position.z- startPos;
            roulmaData.roulmaVariables.roulmaCanvasText.totalMetersCountText.text = currentPos.ToString();
        }
    }
}
