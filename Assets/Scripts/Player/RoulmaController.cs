using System.Collections;
using UnityEngine;
using RoulmaAllFunctions;


public class RoulmaController : MonoBehaviour
{

    //RoulmaFunctions roulmaFunctions = new RoulmaFunctions(); CANT USE NEW BECAUSE OF MONOBEHAVIOUR

    //protected RoulmaVariables roulmaVariables;
    /*
    [SerializeField]
    protected RoulmaFunctions roulmaFunctions;
    [SerializeField]
    protected roulmaFunctions roulmaFunctions;
    [SerializeField]
    protected roulmaFunctions roulmaFunctions;
    [SerializeField]
    protected Joystick joystick;*/
    public static RoulmaData roulmaData;
    protected RoulmaFunctions roulmaFunctions;
    protected RoulmaAnimations roulmaAnimations;

    public void Start()
    {
        roulmaData = gameObject.GetComponent<RoulmaData>();
        roulmaFunctions = gameObject.GetComponent<RoulmaFunctions>();
        //mapSpawnerSystem = gameObject.GetComponent<MapSpawnerSystem>();
        //roulmaFunctions = gameObject.GetComponent<roulmaFunctions>();
        //roulmaFunctions = gameObject.GetComponent<roulmaFunctions>();      
        float turnForce = roulmaData.roulmaVariables.roulmaSpeedVariables.turnForce;
        float turnSpeed = roulmaData.roulmaVariables.roulmaSpeedVariables.turnSpeed;
        roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody = GetComponent<Rigidbody>();
        //roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.centerOfMass = roulmaData.roulmaVariables.roulmaSpeedVariables.centerOfMass.transform.position;
        roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.AddRelativeForce(Vector3.forward * roulmaData.roulmaVariables.roulmaSpeedVariables.accelerationSpeed * 1);
        roulmaData.roulmaVariables.roulmaCanvasButtons.replayTheGameButton.onClick.AddListener(roulmaFunctions.replayTheGame);
        roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaStartPosZ = transform.position.z;
        StartCoroutine(delayBeforeStartGame());
        //roulmaVariables.roulmaCanvasButtons.TurnRoulmaLefttButton.onClick.AddListener(moveTheRoulmaLeftButton);
        //roulmaVariables.roulmaCanvasButtons.TurnRoulmaRightButton.onClick.AddListener(moveTheRoulmaRightButton);
    }

    void FixedUpdate()
    {
        if (!roulmaData.roulmaVariables.roulmaPlayerBooleans.playerIsDead && roulmaData.roulmaVariables.roulmaPlayerBooleans.startGameDelayaIsOver)
        {
            roulmaFunctions.moveTheRoulma();
        }
    }

    private void LateUpdate()
    {
        roulmaFunctions.CalculateSpeed();
        roulmaFunctions.CalculateRpm();
        roulmaFunctions.CalculateMeters();
        roulmaData.roulmaVariables.roulmaCanvasText.speedometerText.text = "Speed: " + roulmaData.roulmaVariables.roulmaSpeedVariables.realTimeSpeed + "Km/h";
        //roulmaVariables.roulmaCanvasText.rpmText.text = "RPM: " + roulmaVariables.roulmaSpeedVariables.rpm;
        roulmaData.roulmaVariables.roulmaCanvasText.rpmText.text = (1.0f / Time.deltaTime).ToString();
    }

    private void shakeTheRoulma()
    {
        float randomShakerNumber = Random.Range(-500, 500);
        roulmaData.roulmaVariables.roulmaSpeedVariables.roulmaRigidbody.AddRelativeForce(Vector3.right * randomShakerNumber, ForceMode.Impulse);
        //roulmaVariables.roulmaRigidbody.AddRelativeForce(Vector3.right * -randomShakerNumber, ForceMode.Impulse);
    }

    IEnumerator delayBeforeStartGame()
    {
        float waitForIt = 1.0f;
        yield return new WaitForSeconds(waitForIt);
        roulmaData.roulmaVariables.roulmaCanvasText.startGameDelayText.text = "1";
        yield return new WaitForSeconds(waitForIt);
        roulmaData.roulmaVariables.roulmaPlayerBooleans.startGameDelayaIsOver = true;
        roulmaData.roulmaVariables.roulmaCanvasText.startGameDelayText.text = "GO!!";
        yield return new WaitForSeconds(waitForIt);
        roulmaData.roulmaVariables.roulmaCanvasText.startGameDelayText.text = "";
    }

    private void OnCollisionEnter(Collision other)
    {
        if (roulmaData.roulmaVariables.roulmaPlayerStats.totalOfLife != 0)
        {
            if (other.gameObject.CompareTag("Coin"))
            {
                Destroy(other.gameObject);
                roulmaFunctions.addOneCoinToTotal();
                roulmaFunctions.destroyCoinAnimation(other.gameObject.transform.position);
            }
            else if (other.gameObject.CompareTag("Bomb"))
            {
                Destroy(other.gameObject);
                roulmaFunctions.removeOneLifeFromTotal();
                roulmaFunctions.destroyBombAnimation(other.gameObject.transform.position);
                roulmaFunctions.activatePreDestroyAnimation(other.gameObject.transform.position);
            }
            else if (other.gameObject.CompareTag("Life") && roulmaData.roulmaVariables.roulmaPlayerStats.totalOfLife < 2)
            {
                Destroy(other.gameObject);
                roulmaFunctions.addOneLifeFromTotal();
                roulmaFunctions.destroyHeartAnimation(other.gameObject.transform.position);
                roulmaFunctions.deActivatePreDestroyAnimation(other.gameObject.transform.position);
            }
            else if (other.gameObject.CompareTag("Win"))
            {
                roulmaFunctions.playerWinAnimation();
                roulmaFunctions.winThePlayer();
            }
            else if (other.gameObject.CompareTag("Building"))
            {
                roulmaFunctions.removeOneLifeFromTotal();
                roulmaFunctions.destroyBombAnimation(other.gameObject.transform.position);
            }
            else if (other.gameObject.CompareTag("WhenToSpawnMap"))
            {

            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
    }
}