using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameRateSet : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
            // Make the game run as fast as possible
            if(Application.isMobilePlatform)
            {
            Application.targetFrameRate = 60;
            }       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
